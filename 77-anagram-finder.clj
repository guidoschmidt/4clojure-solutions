(defn anagram-finder [coll]
  (set (map set (filter #(> (count %) 1)
                        (vals
                         (group-by
                          (fn [ref]
                            (filter
                             #(= % (sort (reverse ref)))
                             (map #(sort (reverse %)) coll)))
                          coll))))))

(anagram-finder ["meat" "mat" "team" "mate" "eat"])
(anagram-finder ["veer" "lake" "item" "kale" "mite" "ever"])

(= (anagram-finder ["meat" "mat" "team" "mate" "eat"])
   #{#{"meat" "team" "mate"}})
(= (anagram-finder ["veer" "lake" "item" "kale" "mite" "ever"])
   #{#{"veer" "ever"} #{"lake" "kale"} #{"mite" "item"}})


