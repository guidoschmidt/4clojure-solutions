(defn simple-closure [base]
  (fn [exp] (reduce * (repeat base exp))))

(= 256 ((simple-closure 2) 16),
       ((simple-closure 8) 2))
(= [1 8 27 64] (map (simple-closure 3) [1 2 3 4]))
(= [1 2 4 8 16] (map #((simple-closure %) 2) [0 1 2 3 4]))
