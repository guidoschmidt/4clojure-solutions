(defn least-common-multiple
  ;; Two parameters
  ([a b]
   (* b (/ a ((fn greatest-divisor [a b]
                (loop [dividend a divisor b]
                  (if (= 0 (mod dividend divisor))
                    divisor
                    (recur divisor (mod dividend divisor))))) a b))))
  ;; More than two parameters
  ([a b & rest]
   (reduce least-common-multiple (least-common-multiple a b) rest)))


(== (least-common-multiple 2 3) 6)
(== (least-common-multiple 5 3 7) 105)
(== (least-common-multiple 1/3 2/5) 2)
(== (least-common-multiple 3/4 1/6) 3/2)
(== (least-common-multiple 7 5/7 2 3/5) 210)


