(defn flip-arguments [func]
  (fn [a b] (func b a)))

(= 3 ((flip-arguments nth) 2 [1 2 3 4 5]))
(= true ((flip-arguments >) 7 8))
(= 4 ((flip-arguments quot) 2 8))
(= [1 2 3] ((flip-arguments take) [1 2 3 4 5] 3))
