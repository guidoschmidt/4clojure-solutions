;; & will bind all the rest of the destructured object
;; to the followed element x
(let [[a b & x] ["cat" "dog" "bird" "fish"]]
  [a x])


(def solution [1 2 3 4 5])

(= [1 2 [3 4 5] [1 2 3 4 5]]
   (let [[a b & c :as d] solution] [a b c d]))


