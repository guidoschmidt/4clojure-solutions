(defn factorial [n]
  (loop [fac n i n]
    (if (= i 1)
      fac
      (recur (* fac (- i 1)) (dec i)))))

(= (factorial 1) 1)
(= (factorial 3) 6)
(= (factorial 5) 120)
(= (factorial 8) 40320)
