(defn symmetric-tree? [[_ [root-a child-a-left child-a-right]
                          [root-b child-b-left child-b-right]]]
  (and (= root-a root-b)
       (or (= nil child-a-left child-b-left)
           (and
            (symmetric-tree? [_ child-a-left child-b-right])
            (symmetric-tree? [_ child-a-right child-b-left])))))

(= (symmetric-tree?
    '(:a
      (:b nil nil)
      (:b nil nil)))
   true)

(= (symmetric-tree?
    '(:a
      (:b
       nil
       nil)
      nil))
   false)

(= (symmetric-tree?
    '(:a
      (:b
       nil
       nil)
      (:c
       nil
       nil)))
   false)

(= (symmetric-tree?
    [1
     [2
      nil
      [3
       [4
        [5 nil nil]
        [6 nil nil]]
       nil]]
     [2
      [3
       nil
       [4
        [6 nil nil]
        [5 nil nil]]]
      nil]])
   true)

(= (symmetric-tree?
    [1
     [2
      nil
      [3
       [4
        [5 nil nil]
        [6 nil nil]]
       nil]]
     [2
      [3
       nil
       [4
        [5 nil nil]
        [6 nil nil]]]
      nil]])
   false)

(= (symmetric-tree?
    [1
     [2
      nil
      [3
       [4
        [5 nil nil]
        [6 nil nil]]
       nil]]
     [2
      [3
       nil
       [4
        [6 nil nil]
        nil]]
      nil]])
   false)


