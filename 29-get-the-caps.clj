(defn caps-only [string]
  (reduce str (re-seq #"[A-Z]" string)))

(= (caps-only "HeLlO, WoRlD!") "HLOWRD")
