(defn prime? [n]
  (cond
    (< n 2) false
    (= n 2) true
    (= n 3) true
    :else (loop [p (dec n)
                 limit (int (Math/sqrt n))]
            (if (>= p limit)
              (if (= 0 (mod n p))
                false
                (recur (dec p) limit))
              true))))

(prime? 1)
(prime? 2)
(prime? 3)
(prime? 5)
(prime? 8)
(prime? 9)
(prime? 10)
(prime? 11)
(prime? 25)
(prime? 541)

(defn prime-numbers [n]
  (take n (filter #(prime? %) (drop 2 (range)))))

(prime-numbers 10)


;; Combined functions
(defn prime-numbers [n]
  (let [prime? (fn prime? [n]
                 (cond
                   (< n 2) false
                   (= n 2) true
                   (= n 3) true
                   :else (loop [p (dec n)
                                limit (int (Math/sqrt n))]
                           (if (>= p limit)
                             (if (= 0 (mod n p))
                               false
                               (recur (dec p) limit))
                             true))))]
    (take n (filter prime? (drop 2 (range))))))

(= (prime-numbers 2) [2 3])
(= (prime-numbers 5) [2 3 5 7 11])
(= (last (prime-numbers 100)) 541)
