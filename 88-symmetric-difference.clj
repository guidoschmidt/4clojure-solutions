(defn symmetric-difference [set-a set-b]
  (let [union (clojure.set/union set-a set-b)
        intersection (clojure.set/intersection set-a set-b)]
    (clojure.set/difference union intersection)))

(= (symmetric-difference #{1 2 3 4 5 6} #{1 3 5 7}) #{2 4 6 7})
(= (symmetric-difference #{:a :b :c} #{}) #{:a :b :c})
(= (symmetric-difference #{} #{4 5 6}) #{4 5 6})
(= (symmetric-difference #{[1 2] [2 3]} #{[2 3] [3 4]}) #{[1 2] [3 4]})
