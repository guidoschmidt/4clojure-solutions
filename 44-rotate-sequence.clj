(defn rotate-seq [dir seq]
  (let [move (if (< dir 0)
               (+ 1 (Math/abs dir))
               dir)]
    (take (count seq) (drop move (apply concat (repeat seq))))))

(= (rotate-seq 2 [1 2 3 4 5]) '(3 4 5 1 2))
(= (rotate-seq 6 [1 2 3 4 5]) '(2 3 4 5 1))
(= (rotate-seq 1 '(:a :b :c)) '(:b :c :a))
(= (rotate-seq -2 [1 2 3 4 5]) '(4 5 1 2 3))
(= (rotate-seq -4 '(:a :b :c)) '(:c :a :b))
