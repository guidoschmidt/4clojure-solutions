(defn fib [n]
  (loop [col '() i 0]
    (cond
      (= i n) col
      (= i 0) (recur (concat col '(1)) (inc i))
      (= i 1) (recur (concat col '(1)) (inc i))
      :else (recur
             (concat col (list (+ (nth col (- i 1)) (nth col (- i 2)))))
             (inc i)))))

(fib 6)
(= (fib 3) '(1 1 2))
