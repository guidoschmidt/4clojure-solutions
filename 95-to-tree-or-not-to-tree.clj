;; TODO: try clojure.zip

(defn binary-tree? [root]
  ;; Check if the root is nil
  (or (nil? root)
      ;; Check if root implements sequential
      (and (sequential? root)
           ;; Check if the root contains 3 elements
           ;; -> parent + 2 children
           (= 3 (count root))
           ;; Test the rest of the root collection
           ;; -> child 1 and child 2
           (every? binary-tree? (rest root)))))

(= (binary-tree? '(:a (:b nil nil) nil))
   true)
(= (binary-tree? '(:a (:b nil nil)))
   false)
(= (binary-tree? [1 nil [2 [3 nil nil] [4 nil nil]]])
   true)
(= (binary-tree? [1 [2 nil nil] [3 nil nil] [4 nil nil]])
   false)
(= (binary-tree? [1 [2 [3 [4 nil nil] nil] nil] nil])
   true)
(= (binary-tree? [1 [2 [3 [4 false nil] nil] nil] nil])
   false)
(= (binary-tree? '(:a nil ()))
   false)
