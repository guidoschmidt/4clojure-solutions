(defn solution [& params]
  (if (= 1 (count (set params)))
    false
    (contains? (set params) true)))

(= false (solution false false))
(= true (solution true false))
(= false (solution true))
(= true (solution false true false))
(= false (solution true true true))
