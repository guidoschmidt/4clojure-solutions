(defn seq-replicate [col n]
  (apply concat
         (map
          (fn [el] (take n (repeat el)))
          col)))

(= (seq-replicate [44 33] 2) [44 44 33 33])
