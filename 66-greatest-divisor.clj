(defn greatest-divisor [a b]
  (loop [dividend a divisor b]
    (if (= 0 (mod dividend divisor))
      divisor
      (recur divisor (mod dividend divisor)))))

(= (greatest-divisor 2 4) 2)
(= (greatest-divisor 10 5) 5)
(= (greatest-divisor 5 7) 1)
(= (greatest-divisor 1023 858) 33)
