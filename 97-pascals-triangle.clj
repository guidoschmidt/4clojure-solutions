(defn pascals-triangle [n]
  "Build the nth row of Pascals triangle."
  (loop [row-index 1
         row [1]]
    ;; If n is reached, return the last calculated row
    (if (= row-index n)
      row
      ;; else recursive call loop with incremented row-index
      ;; and the newly calculated row
      (recur
       (inc row-index)
       ((fn [oldrow]
          (vec (concat [1]
               (map-indexed #(+ %2 (oldrow (+ %1 1)))
                            (take (- (count oldrow) 1) oldrow))
               [1])))
        row)))))


(= (pascals-triangle 1) [1])
(= (map pascals-triangle (range 1 6))
   [     [1]
        [1 1]
       [1 2 1]
      [1 3 3 1]
     [1 4 6 4 1]])
(= (pascals-triangle 11)
   [1 10 45 120 210 252 210 120 45 10 1])

